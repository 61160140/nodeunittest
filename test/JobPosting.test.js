const assert = require('assert')
const { describe } = require('mocha')
const { checkEnableTime } = require("../JobPosting")

describe('JobPosting', () => {
  it('Case:1 เวลาสมัครอยู่ระหว่างเริ่มต้นและสิ้นสุด TRUE', () => {
    // Arrange
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 3)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case:2 เวลาสมัครเท่ากับเริ่มต้น TRUE', () => {
    // Arrange
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 31)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case:3 เวลาสมัครเท่ากับสิ้นสุด TRUE', () => {
    // Arrange
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 5)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case:4 เวลาสมัครน้อยกว่าเริ่มต้น FALSE', () => {
    // Arrange
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 30)
    const expectedResult = false
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('Case:5 เวลาสมัครมากกว่าสิ้นสุด FALSE', () => {
    // Arrange
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 6)
    const expectedResult = false
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
})

